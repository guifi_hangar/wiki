<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents** 

- [upgrade cacti from 0.8.8h to 1.1.38 (latest) in debian 9](#upgrade-cacti-from-088h-to-1138-latest-in-debian-9)
  - [cacti](#cacti)
  - [spine](#spine)
  - [interesting utilities found](#interesting-utilities-found)
- [upgrade cacti from older debian to new debian](#upgrade-cacti-from-older-debian-to-new-debian)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# upgrade cacti from 0.8.8h to 1.1.38 (latest) in debian 9

a way to control cacti version through the compilation of git repos

## cacti

build cacti

cd /usr/local/src
git clone https://github.com/Cacti/cacti
git checkout release/1.1.38

adapt location in apache config file in `/etc/apache2/conf-enabled/cacti.conf`

```diff
-Alias /cacti /usr/share/cacti/site
+Alias /cacti /usr/local/src/cacti

-<Directory /usr/share/cacti/site>
+<Directory /usr/local/src/cacti>
```

linking config file to cacti repo

    cd /usr/share/local/src/cacti/include/
    ln -s /etc/cacti/debian.php config.php
    # database_type must be mysql, with mysqli you will have a FATAL error cannot connect to DB

adaptations that must be done to continue with install/migration. in `/etc/php/7.0/apache2/php.ini` put `date.timezone = "Europe/Madrid"` (or your timezone)

restart apache2

    service apache2 restart

according to https://github.com/Cacti/cacti/issues/361, this is required:

    mysql -u root -p
    use mysql
    GRANT SELECT ON mysql.time_zone_name TO cactiuser@localhost;
    flush privileges

and put data on it

    mysql -u root mysql < /usr/share/mysql/mysql_test_data_timezone.sql

requirements that installation says

    apt install php7.0-gmp php7.0-ldap # needed to install these two (may vary your situation)

restart to count with that dependencies

    service apache2 restart

performance improvements suggested by cacti install `/etc/mysql/my.cnf`

```conf
[mysqld]
max_heap_table_size=49M
max_allowed_packet=16777216
tmp_table_size=64M
join_buffer_size=64M
innodb_buffer_pool_size=244M
innodb_doublewrite=OFF
innodb_additional_mem_pool_size=80M
innodb_flush_log_at_timeout=3
innodb_read_io_threads=32
innodb_write_io_threads=16
```

restart service to apply changes

    service mariadb restart

move graph data

    cp -pa /var/lib/cacti/rra/* /usr/local/src/cacti/rra/

update `/etc/cron.d/cacti` info :

```diff
-*/5 * * * * www-data php --define suhosin.memory_limit=512M /usr/share/cacti/site/poller.php 2>&1 >/dev/null | if [ -f /usr/bin/ts ] ; then ts ; else tee ; fi >> /var/log/cacti/poller-error.log
+*/5 * * * * www-data php --define suhosin.memory_limit=512M /usr/local/src/cacti/poller.php 2>&1 >/dev/null | if [ -f /usr/bin/ts ] ; then ts ; else tee ; fi >> /var/log/cacti/poller-error.log
```

check /etc/cacti/spine.conf DB_pass matches the config.php

## spine

we require to use the same version of cacti and spine (as stated [here](https://forums.cacti.net/viewtopic.php?f=21&t=56943)). both versions of spine cannot coexist (or it would be tricky because the path search cacti does in poller.php)

    sudo apt remove cacti-spine

build spine

    cd /usr/local/src
    apt-get build-dep cacti-spine
    apt install build-essential dos2unix dh-autoreconf help2man libssl-dev libmysql++-dev libmariadb-dev libmariadbclient-dev librrds-perl libsnmp-dev
    git clone https://github.com/Cacti/spine
    cd spine
    git checkout release/1.1.38
    ./bootstrap
    ./configure
    make
    make install

cacti wants to find the path here:

    cd /usr/sbin/spine
    ln -s /usr/local/spine/bin/spine .

check that spine is in the appropriate and same version as cacti

    spine --version

useful resource for compilation https://www.howtoforge.com/tutorial/install-cacti-on-debian-9/

## interesting utilities found

in `/usr/local/src/cacti/cli` directory there is:

repair_templates.php

upgrade_database.php

# upgrade cacti from older debian to new debian

stop cronjobs, mysqldump and put .sql in the new server as suggested by http://xmodulo.com/migrate-cacti-server.html

in the new machine

    apt-get install cacti cacti-spine

(everything default)

```
mariadb -u root

DROP DATABASE cacti;
CREATE DATABASE cacti;
GRANT ALL PRIVILEGES ON cacti.* TO cacti@localhost IDENTIFIED BY "passwordhere";
FLUSH PRIVILEGES;
```

`passwordhere` must match with auth for cacti: /usr/share/cacti/site/include/config.php

after that: `mysql -u cacti -p cacti < cactidump.sql`
inspired by restore procedure -> src https://www.urban-software.com/cacti-howtos/cacti/backup-guide-for-cacti/

access via web, select "upgrade from"

continue with RDD backup/restore as specified in http://xmodulo.com/migrate-cacti-server.html

extra: http://www.cacti.net/downloads/docs/html/upgrade.html
