# accel-ppp

## installation

guide inspired by [Compilando Accel-PPP - Debian 9 Stretch](https://www.youtube.com/watch?v=DpWAidcy5KY). Did not follow the cmake arguments in 18:05, and got a compilation error because it was missing `libssl1.0-dev` (so not `libssl-dev` as stated in youtube video guide) [source of solution](https://accel-ppp.org/forum/viewtopic.php?t=756).

everything did as root account in its ~

dependencies

    apt-get install build-essential cmake git make libpcre3-dev liblua5.1-dev libsnmp-dev linux-headers-amd64 libssl1.0-dev

clone repo. note: https://sourceforge.net/p/accel-ppp/code/ci/master/tree/

    cd /usr/local/src
    git clone https://git.code.sf.net/p/accel-ppp/code accel-ppp
    cd accel-ppp
    #git checkout 1.11.1
    # we got segmentation faults! so we are at the moment in master 938bad2 (2018-08-24)
    git checkout 1.11.1

steps from `README` in git repo

    mkdir build
    cd build
    # changes from cmake in official README file:
    #   enabled SNMP (we currently use/need it)
    #   disable radius to use chapsecrets -> https://accel-ppp.org/wiki/doku.php?id=configsoho
    #   -DKDIR is to specify linux source to compilation of pptp (at the moment we don't want this, just simple: l2tp)
    #   -DCPACK_TYPE=Debian8 to specify a debian system / where is DCMAKE (linux headers) -> https://accel-ppp.org/forum/viewtopic.php?t=985
    # extra: build options explained -> https://accel-ppp.org/wiki/doku.php?id=compilation#build_options
    # after commit f745befbea4d09fb47d5172d58f5b4a162d18977 you can use -DCPACK_TYPE=Debian9
    # DCPACK_TYPE is not working, so let's do a more manual install
    cmake [-DBUILD_DRIVER=TRUE] [-DCMAKE_INSTALL_PREFIX=/usr/src/linux-headers-$(uname -r)] [-DCMAKE_BUILD_TYPE=Release] [-DLOG_PGSQL=FALSE] [-DSHAPER=FALSE] [-DRADIUS=FALSE] [-DNETSNMP=TRUE] ..
    make
    make install
    # debian magic does not work, that's why we did make install
    # # # build deb package (because contains systemd service) -> as suggested by https://blog.remontti.com.br/2538
    # # cpack -G DEB
    # # apt install ./accel-ppp-1.11.0-Linux.deb # or dpkg -i ./accel-ppp-1.11.0-Linux.deb
    # # copy init.d script
    cp ../contrib/debian/accel-ppp-init /etc/init.d/accel-ppp
    cp ../contrib/debian/accel-ppp.service /etc/systemd/system/multi-user.target.wants/
    systemctl enable accel-ppp

after compilation/installation accel-ppp official README suggests to:

    man accel-ppp.conf

a sample config file is in /etc/accel-ppp.conf.dist

but this config file looks very fine https://accel-ppp.org/wiki/doku.php?id=configsoho

copy configsoho config file, that way you can start accel-ppp service

    service accel-ppp start

ipv6 pool https://accel-ppp.org/wiki/doku.php?id=configfile#ipv6-pool

~~apply logrototate suggested by [FAQ](root@bng:/var/log/accel-ppp# cat /etc/logrotate.d/accel-ppp )~~ I don't like its suggestion, got inspiration [here](https://github.com/savonet/liquidsoap/blob/master/scripts/liquidsoap.logrotate.in)

```
/var/log/accel-ppp/*.log {
        compress
        rotate 5
        size 300k
        missingok
        notifempty
        sharedscripts
        postrotate
                test -r /var/run/accel-pppd.pid && kill -HUP `cat /var/run/accel-pppd.pid`
        endscript
}
```

extra https://serverfault.com/questions/574121/is-it-possible-for-l2tp-vpn-to-do-auto-route-configuration-for-client-during-con
