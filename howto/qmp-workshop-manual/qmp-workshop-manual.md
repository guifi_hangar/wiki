---
author: 'guifi.net community'
title: 'quick Mesh project (qMp.cat) workshop'
---

Introduction
============

The basic material to deploy qMp networks.

The devices used have Power over Ethernet (PoE) of 24 V, it means that
the electrical power and data of the device goes through an ethernet
cable. The blue cable in figure <fig:poe> is what is deployed into an
outdoor place (typically rooftop or a balcony) where the qMp devices
will be installed and takes the PoE interface in PoE injector. The other
interface in the PoE injector is LAN [^1] and can be placed to a PC or a
switch/router if it is wanted to connect more PC's. Finally, the PoE
injector requires standard electrical power. **Warning**: a PoE
connection to a device that is not prepared to work with PoE 24 V, for
example an ethernet interface of a computer, can produce malfunction of
its ethernet interface.

![PoE network diagram](./img/general/poe.jpg "poe")

Installer basics: Items
=======================

-   Outdoor cable and rj45 connectors (sold separately)\
    The cable should be for outdoor to resist the weather.
    -   UTP cable: cable without protection. But sometimes it is enough.
    -   FTP cable (recommended): cable with basic protection. ESD
        prevention [^2]. The difference is that the cable has ground.
-   Crimp tool: to join rj45 connector to cable.
-   Cable tester (recommended): a unexpensive one can help to work
    quickly and reliably in the installation of cable.
-   Cable cutter: To take a segment of cable for a installation. Note:
    UTP cable is easier to cut than FTP.
-   Cable tie: To lock the cable & device to a antenna pole and
    other cables.
-   Shopping cart or traveling bag with wheels (recommended): carry all
    the items needed.
-   Measure meter: measure accurately the length of cable
    needed (optional).

Installer basics: 5 GHz devices
===============================

The devices selected are working in the 5 GHz band because 2.4 GHz is
widely used in cities and have more interferences. They are from
Ubiquiti manufacturer and compatible with qMp firmware.

Unexpensive

:   around 70 euro.

    NanoStation Loco M5 (NSLM5)
    :   Short distances (less than 1 km). The connection with the other
        candidate node has an acceptable connection and there is no need
        to increase the power signal of wifi. It uses the same firmware
        as NanostationM5.

    Nanostation M5 (NSM5)

    :   

    NanoBeam M5 (NBM5) different models

    :   

    XW series
    :   Are new versions of devices NSLM5, NSM5, etc. but with
        new processors. The first introduced was NBM5 as a replacement
        to NanoBridge M5 (now deprecated). This devices requires qMp 4
        and is not stable yet. The rest of devices specified are
        AirMax (XM) and use qMp 3.1 (stable release).

Expensive

:   between 100 and 300 euro.

    Rocket M5

    :   

Summary of some relevant information at Table <tab:devspec>:

  ----------------- ---------------- --------------------- ------- ------ -------
  Devices           Gain (dBi)       Beamwidth (deg)       Proc.   RAM    Flash
                                     Hpol/Vpol/Elevation           (MB)   (MB)
  NSLM5             13               45/45/45              24KC    32 S   8
  NSM5              16               43/41/15              24KC    32 S   8
  NBM5              16, 19, 22, 25   see on datasheet      74KC    64 D   8
  XW series         -                -                     74KC    -      -
  RM5 S90 MG, HG    17, 20           see on datasheet      24KC    64 S   8
  RM5 S120 MG, HG   16, 19           see on datasheet      24KC    64 S   8
  RM5 D             30, 34           see on datasheet      24KC    64 S   8
  ----------------- ---------------- --------------------- ------- ------ -------

  : Devices specifications

Proc

:   Processor Specs.

    24KC
    :   Atheros MIPS 24KC, 400MHz

    74KC
    :   Atheros MIPS 74KC, 560MHz

RAM

:   Type of RAM:

    S
    :   SDRAM

    D
    :   DDR2

qMp basics: Testing operations
==============================

Figure <fig:wan-status-on> shows the first screen obtained when there is
a log in a qMp node.

![First
screen](./img/qMp-basics-scrot/status-wan_status.png "wan-status-on")

\noindent

To come back to this screen, go to the menu clicking at:

``` {.example}
qMp/Mesh / Status
```

alternatively:\
<http://admin.qmp/cgi-bin/luci/qmp/status>

When there is a scroll down action, appears Associated Stations. Figure
<fig:associated-stations> has the wifi links with other qMp nodes and
what signal associated (dBm). The guifi.net good practices says that the
backbone should be better than -75dBm [^3]. In that figure there are
different kind of links with different qualities. Good quality means
high parameters of: dBm, RX Rate, TX Rate \[bandwidth (Mbps)\] and MCS
codification (the number).

These qualities refer to connection to different nodes, only is shown
the MAC address. But the MAC is enough to identify a node, because the
last four characters are appended in every hostname of the network.
Later, it will be known how to navigate to different nodes in the
network.

![Associated
stations](./img/qMp-basics-scrot/status-associated-nodes.png "associated-stations")

Another measure of quality is shown on Figure <fig:links-node>. This is
the quality in terms of the protocol bmx6. A 0-100 rating in terms of
reception and transmission (rx/tx).

\noindent

To arrive there, go to the menu clicking at:

``` {.example}
qMp/Mesh / Mesh / Links
```

alternatively:\
<http://admin.qmp/cgi-bin/luci/qmp/mesh/links>

![Links of the node](./img/qMp-basics-scrot/links.png "links-node")

Also, can be made a bandwidth test between nodes. Figure <fig:bw-test>
perform a TCP connection benchmark and give the Mbps between the node
and other possible destinations. Wait until a single test ends to know
all the bandwidth in the link or route.

\noindent

To arrive there, go to the menu clicking at:

``` {.example}
qMp/Mesh / Tools
```

alternatively:\
<http://admin.qmp/cgi-bin/luci/qmp/tools>

![Bandwidth Test](./img/qMp-basics-scrot/test-bandwidth.png "bw-test")

Figure <fig:wifi-signal-rt>. After the general scan, when there is a
node candidate to do a durable connection, there is the need to analyse
the quality of this link in real-time. This helps to select an optimised
place to lock the device in the installation.

\noindent

To arrive there, go to the menu clicking at:

``` {.example}
OpenWRT / Status / Realtime Graphs / Wireless
```

alternatively:\
<http://admin.qmp/cgi-bin/luci/admin/status/realtime/wireless>

![Strength of the best wifi signal in
real-time](./img/qMp-basics-scrot/realtime_wifi_link.png "wifi-signal-rt")

The situation could be that cannot be a connection to the node to the
network. Perhaps it is in another channel. Figure <fig:find-qmp> shows a
wifi scan. qMp always use BSSID: `02:CA:FF:EE:BA:BE`, in Mode `Ad-Hoc`.
These are two solid references to find other qMp networks. In the figure
there are two qMp networks in channels: 140 and 132.

\noindent

To arrive there, go to the menu clicking at:

``` {.example}
OpenWRT / Network / Wifi / "Scan"
```

alternatively:\
<http://admin.qmp/cgi-bin/luci/admin/network/wireless> and click Scan.

![Wifi scan: find qMp
network](./img/qMp-basics-scrot/wifi_scan_find_qmp.png "find-qmp")

If there is a design of a new qMp network it is important to select a
channel that is not used. Figure <fig:interference> shows how another AP
is also using channel 140.

![Wifi scan
interference](./img/qMp-basics-scrot/wifi_scan_interference.png "interference")

Figure *wifi-channel-power* shows where to change wifi parameters as
wifi channel and power signal to the qMp network. By default, qMp uses
17, but it can be increased to 22 (max value).

Use the transmission power of wifi signal with care, in the interested
network is a communication signal, but for the other networks it is
another noise in the environment that make its communications more
difficult.

\noindent

To arrive there, go to the menu clicking at:

``` {.example}
OpenWRT / Node configuration / Wireless Settings
```

alternatively:\
<http://admin.qmp/cgi-bin/luci/qmp/configuration/wifi/>

![Wifi: Channel &
Power](./img/qMp-basics-scrot/wifi-channel-power.png "wifi-channel-power")

Figure <fig:tunnels> (marked as red) shows that there is a WAN Node, the
node makes announcement of this network as `Internet`. If can be arrived
there, it means there is an internet connection, try it with a browser.
Also could be interesting to perform additional bandwidth tests [^4]
[^5] [^6] [^7].

But perhaps the WAN node cannot be accessed, or there is not a WAN node
in the network. Can be checked if there is a tunnel to Internet.

In the same view, can be browsed for a Border Node. Figure <fig:tunnels>
shows it (marked as blue), the node makes announcement of the network
`10.0.0.0/8`, it means, access to the rest of guifi.net

\noindent

To arrive there, go to the menu clicking at:

``` {.example}
qMp/Mesh / Mesh / Tunnels
```

alternatively:\
<http://admin.qmp/cgi-bin/luci/qmp/Mesh/Tunnels>

![Tunnels](./img/qMp-basics-scrot/tunnels.png "tunnels")

qMp basics: Basic install and maintaining
=========================================

Figure <fig:quick-setup>, this is the final setup when the node is
prepared to be in testing phase.

In guifi.net web page, after adding the device, it is received a unique
ip address, and is needed a `255.255.255.244` netmask. Use the same name
as in the web or the network organization page.

\noindent

To arrive there, go to the menu clicking at:

``` {.example}
qMp/Mesh / Node configuration / qMp easy setup
```

alternatively:\
<http://admin.qmp/cgi-bin/luci/qmp/configuration/easy_setup/>

![Quick setup](./img/qMp-basics-scrot/quick_setup.png "quick-setup")

Figure <fig:backup>: When the node is working fine is important to make
a backup of the configuration. It is not recommended to upgrade the node
using this menu for the qMp firmware.

\noindent

To arrive there, go to the menu clicking at:

``` {.example}
OpenWRT / System / "Backup/Flash Firmware"
```

alternatively:\
<http://admin.qmp/cgi-bin/luci/admin/system/flashops>

![Backup](./img/qMp-basics-scrot/backup-new-firmware.png "backup")

For upgrade the node at the moment it is only possible via terminal. Do
a login with ssh session:

``` {.example}
ssh root@admin.qmp
```

password: 13f\
From this point, there are three methods:

1.  Automatic upgrade (with internet connection in the node).

    ``` {.example}
    qmpcontrol upgrade
    ```

2.  Upgrade with a link (with internet connection in the node).

    ``` {.example}
    qmpcontrol upgrade "http://...qmp.bin"
    ```

    It means the URL where is located the qMp firmware, remember that
    can be found all the firmwares supported here: <http://fw.qmp.cat>
3.  Upgrade with a local file (without internet connection in the node).
    1.  Put the file inside qMp node, open a new terminal and put

        ``` {.example}
        scp qmp.bin root@admin.qmp:/tmp
        ```

        It will ask for the password
    2.  With the existing ssh session opened before, or a new one, login
        with ssh and:

        ``` {.example}
        qmpcontrol upgrade "/tmp/qmp.bin"
        ```

Confirm to continue with the upgrade process and wait until it is
finished.

Note: qMp only save common settings after the upgrade, concretely:

``` {.example}
# cat /etc/config/qmp | grep preserve
```

For other file changes, perform a backup before the upgrade.

qMp basics: Navigating inside the network
=========================================

Figure <fig:net-nodes> shows a screen that presents all the qMp nodes
conforming the network. By clicking the blue spherical icon to the left
of each node it is possible to obtain additional information about them.
In particular, the network address announced by one node can be found
under the `Gateways announced` label, and the IP of the node in the
first address of that network. In the example shown in the figure, the
network address is `10.1.56.96` and the IP of the qMp node is
`10.1.56.97`.

\noindent

To arrive there, go to the menu clicking at:

``` {.example}
qMp/Mesh / Mesh / Nodes
```

alternatively:\
<http://admin.qmp/cgi-bin/luci/qmp/mesh/nodes>

![IP address of
nodes](./img/qMp-basics-scrot/net-of-nodes.png "net-nodes")

Figure <fig:graph-network> is the graph that shows the nodes, the edges
with the bmx6's quality rate show how each are connected.

\noindent

To arrive there, go to the menu clicking at:

``` {.example}
qMp/Mesh / Mesh / Graph
```

alternatively:\
<http://admin.qmp/cgi-bin/luci/qmp/mesh/graph>

![Graph of the
network](./img/qMp-basics-scrot/graph.png "graph-network")

Proposed qMp network node designs: WAN node design
==================================================

To build a WAN node, figure <fig:wan-gen> shows how the qMp node should
be connected to the *mesh* network (through wifi via bmx6 routing) and
Internet (through ethernet to ISP [^8] router via DCHP client).

It is recommended to use the device Nanostation M5 because it has two
ethernet interfaces (eth0, eth1). With one can be made a DHCP server to
connect to the qMp node with a laptop. And for the other ethernet, a
DHCP client to the ISP router.

In the case that there is a Nanostation Loco M5, it only has one
ethernet (eth0 [^9]). It will be for the DHCP client to the ISP router
and it means that there is no DHCP server to directly connect to the qMp
node. An easy solution is that the connection to the qMp node could be
possible with another qMp node in the network (it is being used the wifi
interface).

![Network diagram generic WAN
node](./img/mesh-designs/wan_node_generic.png "wan-gen")

To set the ethernet that will do the DHCP client to the ISP router there
are 2 options.

Option 1: in the quick setup, last part says what to do with interfaces
(figure <fig:quickdhcp>). The interfaces have 3 selections: `Mesh`,
`Lan` (DHCP server) and `WAN` (DHCP client).

![Option 1: Set DHCP client to interface with quick
setup](./img/qMp-basics-scrot/quick_setup_interfaces.png "quickdhcp")

Option 2: Figure <fig:netset> shows the screen that set the DHCP client
interface, and there is no need to do a quick setup with the node.

\noindent

To arrive there, go to the menu clicking at:

``` {.example}
OpenWRT / Node configuration / Network Settings
```

alternatively:\
<http://admin.qmp/cgi-bin/luci/qmp/configuration/network/>

![Option 2: Set DHCP client to interface with network
settings](./img/qMp-basics-scrot/network_settings.png "netset")

To test that is working the DHCP client to the ISP router, check the
IPv4 WAN Status, section Network. Figure <fig:wan-status-on-detail>
shows a successful WAN connection. Figure <fig:wan-status-off> shows a
unsuccessful WAN connection: there is no DHCP client or is not correctly
connected.

![WAN status
online](./img/qMp-basics-scrot/status-wan_status_detail.png "wan-status-on-detail")

![WAN status
offline](./img/qMp-basics-scrot/wan_not_connected.png "wan-status-off")

\noindent

To arrive there, go to the menu clicking at:

``` {.example}
qMp/Mesh / Mesh / Status
```

alternatively:\
<http://admin.qmp/cgi-bin/luci/qmp/status>

Proposed qMp network node designs: General node design
======================================================

Figure <fig:gen-node> shows the elements of a simple node installation:
A qMp node connected to its network and a 2.4 GHz wifi router as an
Access Point that it is necessary to give wifi coverage inside the
place.

![Network diagram generic
node](./img/mesh-designs/generic_node.png "gen-node")

Flash qMp node
==============

Steps to flash a device with qMp firmware. It is assumed a GNU/Linux
Ubuntu/Debian computer:

1.  Download the **Factory image** [^10] for a supported device that has
    the factory operating system [^11]. **Sysupgrade image** is for
    OpenWRT or qMp nodes that want to upgrade. **Guifi image** has
    better integration with guifi.net web.
2.  Rename the downloaded file to `qmp.bin`.
3.  Download the tftp packets with the system's repository. In terminal:
    `$ sudo apt-get install tftp-hpa`.
4.  Disconnect the internet connection.
5.  Open a terminal and put:

    ``` {.example}
    $ ping 192.168.1.20
    ```

    It will help to know when the device is in the reset mode.
6.  Connect the equipments as shown in Figure <fig:flashdiagram>.

    ![Network Diagram to Flash
    Device](./img/general/flashdiagram.jpg "flashdiagram")

7.  Configure the network following one of these options:
    1.  **GUI option**: configure in the preferred network manager a
        ethernet network with static IP in the computer to connect to
        the device:\
        IP: 192.168.1.10\
        Subnet: 192.168.1.100\
        Gateway: 192.168.1.1
    2.  **Terminal option**:

        ``` {.example}
        $ sudo ip a a 192.168.1.25/24 dev eth0
        ```

8.  Reset the device following one of these options:
    1.  **Reset in the device option**: Disconnect the interface of
        the device. Remove the device's lid. With one hand take an
        object with round toe, press and hold reset button (Figure
        <fig:resetant>) while with the other hand insert the ethernet
        cable to the interface in device.

        ![Reset device](./img/general/reset-device.jpg "resetant")

    2.  **Reset in the PoE injector option**: Check if the device has
        PoE (Figure <fig:resetpoe>). Disconnect the PoE interface in
        PoE injector. With one hand take an object with round toe, press
        and hold reset button while with the other hand insert the
        ethernet cable to the PoE interface in PoE injector.

        ![Reset power
        injector](./img/general/reset-injector.jpg "resetpoe")

9.  Observe if the device start the reset mode needed for continue:
    -   **Device led option**: Wait until the led 1 and 3 change to 2
        and 4 cyclically. With this video resource it will give an idea
        of time and led colors involved in the process [^12].
    -   **PC screen option**: the ping starts responding. The output of
        the `ping 192.168.1.20` should be something like:

        ``` {.example}
        64 bytes from X: icmp_req=X ttl=X time=X ms
        ```

10. If is in reset mode stop pressing the reset button and put the
    device in a stable place.
11. In a new terminal window, go where is the downloaded firmware
    `qmp.bin`:

    ``` {.example}
    cd /path/to/the/qmpbin_folder
    ```

    And there, execute:

    ``` {.example}
    $ tftp 192.168.1.20
    $ mode octet
    $ trace
    $ put qmp.bin
    ```

    \[ Transmission process \]

    ``` {.example}
    $ quit
    ```

12. After about 5 minutes, the 4th led of the ramp (the most right led,
    Figure <fig:ledsdevice>) is on, and not blinking. This is the moment
    to go the next step.

    ![Led system in the
    device](./img/general/blinkingled.jpeg "ledsdevice")

13. Reconfigure the network to do a DHCP client in ethernet port
    (Automatic IP) and try to connect again the PC with the device.
14. Check that the device responds to ping:

    ``` {.example}
    $ ping 172.30.22.1
    ```

    This is the fixed IP address in roaming mode.\
    More general approach is to get the gateway address:

    ``` {.example}
    $ ip r | grep default | cut -f3 -d' '
    ```

    Open a web browser and check if this web can be accessed
    (**Warning** admin.qmp it will only work if the PC is connected to
    the device via DHCP):

    ``` {.example}
    http://admin.qmp
    ```

    alternatives:

    ``` {.example}
    http://172.30.22.1
    http://<gateway_ip>
    ```

15. Login access is user: root\
    password: 13f

Other references [^13] [^14] [^15]

Making a panorama with Hugin
============================

With Hugin it is very easy to do panorama photos, and is free open
source software [^16].

1.  How to do the photos? Take the same physical point and start doing
    photos with 20% of overlap between them.
2.  Follow the steps in Hugin's program (Figure: <fig:hugin>)
    1.  `1.Load images`, select all images in the folder it is wanted to
        do a panorama.
    2.  `2.Align`.
        -   this takes a process to search for control points for give
            sensation of continuity in the photo.
        -   if there is not enough control points, search control points
            manually or do the photos again.

    3.  `3.Create panorama`: save a .pto and .tiff files in the folder
        with all images.

    ![Hugin](./img/general/hugin.png "hugin")

3.  Conversion of .tiff to .jpeg\
    If it is wanted to share the panorama.

    ``` {.example}
    sudo apt-get install imagemagick
    convert pan.tiff pan.jpeg
    ```

    An example is showed in figure <fig:exhugin>

    ![Example of panorama using
    hugin](./img/santandreudeploy/llenguadoc.jpg "exhugin")

About monitoring
================

Perform a monitoring of the network is important as a measure of quality
assurance. Are presented 3 alternatives.

**From the guifi web**
----------------------

can be obtained the graphs. It helps to know if the device is up, its
ping and the network traffic. Figure <fig:snpservices> shows how it
looks like.

![Graph server in
guifi.net](./img/general/snpservices.png "snpservices")

It is required a qMp version with guifi package: `qMp-Guifi` should
appear in the bin package name.

The server part uses a package developed by guifi.net community called
`snpservices`. For install it can be followed this guide [^17],
basically, a Debian repository is obtained, it is installed the package
and is set the id of the graph server (other parameters remain default).
To obtain the id of the graph server create a service of type graph
server in the guifi.net web. For example, the id of the graph server of
Barcelona can be obtained from the URL:
`http://guifi.net/en/node/55045`, and it is `55045`.

qMp uses the package `mini_snmpd` [^18] configured to the guifi.net
website. After creating the node and the device in the web, it generates
the `unsolclic` file. Figure <fig:qmpguifi> shows how simple is: put
there the URL of the device and apply.

![guifi.net menu in qMp
firmware](./img/qMp-basics-scrot/qmpguifi.png "qmpguifi")

**munin**:
----------

For a GNU/Linux Debian 7 Wheezy server (apache 2.2)

``` {.example}
sudo apt-get install munin
```

by default it does monitoring of the server itself (localhost).

For make the graphs available for every user [^19] in order to follow
the Community Network model of open all network data change the
following lines in `/etc/munin/apache.conf`:

``` {.example}
Order allow,deny
Allow from localhost 127.0.0.0/8 ::1
Options None
```

like so:

``` {.example}
Order allow,deny
Allow from all
Options FollowSymLinks SymLinksIfOwnerMatch
```

Apply the changes in the HTTP server:

``` {.example}
# service apache2 restart
```

Add qMp nodes for monitor them editing the file `/etc/munin/munin.conf`:

``` {.conf}
[qMp-node1] address 10.x.x.x use_node_name yes [qMp-node2] address 10.x.x.x use_node_name yes
```

Apply the changes in the monitor (it will start appearing after few
minutes):

``` {.example}
# service munin-node restart
```

The graphics are very similar to those of guifi, but provide more
information. Except that there is an error with network traffic
monitoring and is not provided.

**qmpsu**:
----------

At the moment, there is not a generic package of qmpsu for qMp networks,
only for Sants Poblenou. More information see
*Situation of mesh networks in Barcelona*. Figure <fig:qmpsu> shows how
it looks like.

![qmpsu view](./img/general/qmpsu.png "qmpsu")

Footnotes
=========

[^1]: LAN cable can make a length up to 100m if only is carrying data

[^2]: <http://en.wikipedia.org/wiki/Electrostatic_discharge>

[^3]: Catalan: <http://guifi.net/ca/BonesPractiquesUER>

[^4]: <http://www.catnix.net/en/speedtest>

[^5]: <http://speedtest.net>

[^6]: <http://testdevelocidad.es>

[^7]: <http://testvelocidad.eu/>

[^8]: Internet Service Provider

[^9]: eth1 is ignored

[^10]: <http://fw.qmp.cat/>

[^11]: <http://qmp.cat/Supported_devices>

[^12]: [https://www.youtube.com/watch?v=xIflE\_-V-B4\\\#t=50s](https://www.youtube.com/watch?v=xIflE_-V-B4\#t=50s)

[^13]: <http://wiki.ubnt.com/Firmware_Recovery>

[^14]: [http://www.qmp.cat/\\\#Use-the-firmware](http://www.qmp.cat/\#Use-the-firmware)

[^15]: tftp info:
    <http://wiki.openwrt.org/doc/howto/generic.flashing.tftp>

[^16]: <http://hugin.sourceforge.net/>

[^17]: There is no English translation:
    <http://ca.wiki.guifi.net/wiki/Servidor_de_gr%C3%A0fiques_1>

[^18]: <http://wiki.openwrt.org/doc/howto/snmp.server>

[^19]: Solution for apache 2.2 and 2.4:
    <http://stackoverflow.com/questions/9127802>
