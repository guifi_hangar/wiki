web: https://fundacio.guifi.net

# Informació de l'operador de telecomunicacions majorista de la Fundació

- 2018-11-8: tenim un ample de banda de 30Gbps i actualment fem pics de fins a 23Gbps. Pots mirar les gràfiques al noc2, però com que des de l'estiu han canviat, costarà trobat consum agregat. Si vols truca'l perquè jo no tinc accés a les gràfiques

- Sistema autònom (AS) de la fundació 49835
    - https://stat.ripe.net/AS49835#tabId=at-a-glance
    - http://as-rank.caida.org/asns/49835
    - https://www.peeringdb.com/asn/49835
    - https://bgpview.io/asn/49835
    - info extra http://es.wiki.guifi.net/wiki/Autonomous_System

# Informació sobre fibra òptica

- estesa de fibra 6€/m inclou TOT (mà d'obra, electrònica, etc.). Hi ha qui diu 10€/m i 100€/m si és obra civil / obrir carrer
- preu fix de 1500€ per llar (a vegades pot ser més alt si costa d'accedir a aquesta llar particularment)

# La importància de les donacions en l'estesa de fibra òptica de comuns

La única forma que la fundació guifi té de garantitzar que la fibra òptica és i serà de comuns és a través dels apadrinaments en forma de donació que els usuaris (organitzacions, individus) fan a la fundació guifi per valor de 1500€ (hi han models de finançament i/o pagament en forma de quotes). Alguns operadors posen uns preus similars però ni informen a l'usuari ni a la fundació guifi de la operació, conseqüentment aquell tram de fibra òptica no és propietat de fundació guifi. Per tant es tracta d'un conflicte d'interès (telco amb ànim de lucre propietari d'un inmoble de comuns que pot privatitzar fàcilment).

Només una puntualització en relació a aquest paràgraf: Si bé és cert que si no es fan donacions la titularitat de la infraestructura és de l'operadora de telecomunicacions, també és cert que aquestes a través del PDSAiDR que presenten a ajuntaments, altres administracions i altres empreses co-ocupadores d'infraestructures abans de realitzar l'estesa fan una declaració responsable indicant que "Aquesta infraestructura, un cop desplegada, esdevindrà un bé comú que es posarà a disposició de tots els operadors comercials en les mateixes condicions neutrals, objectives, transparents, equitatives i no discriminatòries tal i com preveuen els Estatuts de la Fundació i el Comuns XOLN" i, d'altra banda, sempre constem com a promotors del Pla de Desplegament La Fundació. Us n'adjunto un exemple. 
